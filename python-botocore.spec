%global _empty_manifest_terminate_build 0
Name:		python-botocore
Version:	1.29.162
Release:	1
Summary:	Low-level, data-driven core of boto 3.
License:	Apache-2.0
URL:		https://github.com/boto/botocore
Source0:	https://files.pythonhosted.org/packages/47/be/328d6fcb30c4335793d3bcec407e66e576122b01abc0c7f79800c0f37ebd/botocore-1.29.162.tar.gz	
BuildArch:	noarch

Requires:	python3-jmespath
Requires:	python3-dateutil
Requires:	python3-urllib3

%description
A low-level interface to a growing number of Amazon Web Services. The
botocore package is the foundation for the AWS CLI as well as boto3.

%package -n python3-botocore
Summary:	Low-level, data-driven core of boto 3.
Provides:	python-botocore = %{version}-%{release}
BuildRequires:  python3-jmespath
BuildRequires:  python3-dateutil
BuildRequires:  python3-urllib3
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  python3-pytest
%description -n python3-botocore
A low-level interface to a growing number of Amazon Web Services. The
botocore package is the foundation for the AWS CLI as well as boto3.

%package help
Summary:        Low-level, data-driven core of boto 3.
Provides:       python3-botocore-doc
%description help
A low-level interface to a growing number of Amazon Web Services. The
botocore package is the foundation for the AWS CLI as well as boto3.

%prep
%autosetup -n botocore-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
%{__python3} -m pytest tests/unit

%files -n python3-botocore -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Jun 28 2023 niuyaru <niuyaru@kylinos.cn> - 1.29.162-1
- Update package to version 1.29.162

* Tue Jun 27 2023 zixuanchen <chenzixuan@kylinos.cn> - 1.29.161-1
- Update package to version 1.29.161

* Mon May 29 2023 wubijie <wubijie@kylinos.cn> - 1.29.125-1
- Update package to version 1.29.125

* Tue May 16 2023 wubijie <wubijie@kylinos.cn> - 1.29.120-1
- Update package to version 1.29.120

* Wed May 10 2023 wubijie <wubijie@kylinos.cn> - 1.29.114-1
- Update package to version 1.29.114

* Sat May 06 2023 wubijie <wubijie@kylinos.cn> - 1.29.112-1
- Update package to version 1.29.112

* Thu May 04 2023 wubijie <wubijie@kylinos.cn> - 1.29.111-1
- Update package to version 1.29.111

* Tue Apr 11 2023 Ge Wang <wang--ge@126.com> - 1.29.110-1
- Update to version 1.29.110

* Mon Mar 20 2023 wubijie <wubijie@kylinos.cn> - 1.29.94-1
- Update package to version 1.29.94

* Tue Feb 14 2023 wubijie <wubijie@kylinos.cn> - 1.29.70-1
- Update package to version 1.29.70

* Thu Dec 15 2022 liqiuyu <liqiuyu@kylinos.cn> - 1.29.30-1
- Update package to version 1.29.30

* Thu Nov 10 2022 liqiuyu <liqiuyu@kylinos.cn> - 1.29.6-1
- Upgrade package to version 1.29.6

* Fri Oct 14 2022 liqiuyu <liqiuyu@kylinos.cn> - 1.27.90-1
- Upgrade package to version 1.27.90

* Mon Jul 04 2022 OpenStack_SIG <openstack@openeuler.org> - 1.24.7-1
- Upgrade the version to 1.24.7

* Wed Apr 6 2022 caodongxia <caodongxia@h-partners.com> - 1.23.4-1
- Upgrade python-botocore to 1.23.4

* Thu Mar 31 2022 wulei <wulei80@huawei.com> - 1.20.26.2
- Fix rejecting URLs with unsafe characters in is_valid_endpoint_url()

* Mon Jul 26 2021 OpenStack_SIG <openstack@openeuler.org> - 1.20.26-1
- update to 1.20.26

* Mon Nov 16 2020 yanan li <liyanan32@huawei.com> - 1.19.17-1
- Package init

